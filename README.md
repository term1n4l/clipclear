# clipclear

## Overview

Clipclear is a quick automation I created for KDE to automatically clear the clipboard history. It takes the form of a systemd timer and service unit that are run under your user to clear the clipboard on a 15 min interval.

## Configuration

Copy the `clipclear.service` and `clipclear.timer` files to `/usr/lib/systemd/user/`. Run `systemctl --user enable clipclear.timer && systemctl --user start clipclear.timer`.

## How this came about

I was looking through the Klipper settings to see if there was already a way to clear the history on a set time. As there wasn't I went out to determine how one could clear the Klipper history from the cli which led me to [forum.kde.org](https://web.archive.org/web/20230429221844/https://forum.kde.org/viewtopic.php?f=289&t=169780) where someone was asking that same question. I then created the service and timer unit file from there.

## Pre-requisites

- KDE
- qdbus (it was already installed for me so may be a part of the default packages in KDE)
- Klipper

## Contributing

Submit a pull request

## License

> Copyright (C) 2023  Ryan G.

> This program is free software; you can redistribute it and/or
> modify it under the terms of the GNU General Public License
> as published by the Free Software Foundation; either version 2
> of the License, or (at your option) any later version.

> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.

> You should have received a copy of the GNU General Public License
> along with this program; if not, write to the Free Software
> Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

## About the author

I am a linux enthusiast with a passion for automation

